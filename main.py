#!/usr/bin/env python

import os
from storage import storage
from time import sleep

class beahero:
    def __init__(self, ui):
        self.ui = ui
        dir = os.path.join(ui.datadir(),'peacegame2')
        self.player = storage(dir)
        
        if 'level' not in self.player:
            self.player['level'] = 1
            self.player['experience'] = 0
            self.player['friends'] = 0

        self.run()
    def run(self):
        while True:
            self.one()
    def experience_for_level(self, level):
        return int(4**(level/1.7))
    def one(self):
        self.ui.display_block('Stats',
'''You are a level {level} peaceful mediator.
You have {friends} friends.
You\'ve never heard the word "enemy".
You know that nobody is at fault for anything.
You have {experience} experience.  Next level at {nextexperience}.'''.format(nextexperience=self.experience_for_level(self.player["level"]+1), **self.player)
        )
        sleep(0.3)
        self.ui.confirm_to('explore')
        sleep(0.4)
        self.ui.display_block('Encounter', 'You encounter a conflict between 2 people!')
        sleep(0.3)
        self.ui.confirm_to('try to mediate the conflict')
        sleep(0.4)
        self.ui.display_block('Success', '''Congratulations!

The fight dissolves into friendship!''')
        sleep(0.4)
        self.ui.append_block('Everyone is satisfied!')
        sleep(0.4)
        experience_gained=3
        new_friends=2
        self.ui.append_block('You gained {experience_gained} experience and made {new_friends} new friends.'.format(**locals()))
        sleep(0.5)
        self.player['experience'] += experience_gained
        self.player['friends'] += new_friends
        if self.player['experience'] > self.experience_for_level(self.player['level']+1):
            self.player["level"] += 1
            self.ui.display_block('Advancement', 'You are now level {level}!'.format(**self.player))
            sleep(3)
        self.ui.confirm_to('see your stats')

if __name__ == '__main__':
    try:
        from uikivy import ui
        ui()
    except:
        from uicli import ui
        ui()
