from kivy.app import App
from kivy.uix.button import Button
from kivy.uix.screenmanager import ScreenManager, Screen, FadeTransition, NoTransition
import threading

class HeroPath(App):
    def datadir(self):
        return self.user_data_dir
    def build(self):
        self.cv = threading.Condition()
        self.confirm = False
        self.stop = False
        self.sm = ScreenManager(transition = NoTransition())
        self.display_block('Start', '...')
        return self.sm
    def on_start(self):
        import main
        self.mainthread = threading.current_thread()
        self.thread = threading.Thread(target=lambda: main.beahero(self))
        self.thread.start()
    def on_stop(self):
        self.stop = True
        self.cv.acquire()
        self.confirm = True
        self.cv.notify_all()
        self.cv.release()
        self.thread.join()
    def on_pause(self):
        return self.thread.is_alive
    def on_resume(self):
        pass

    def display_block(self, name, message):
        self.message = message
        last = self.sm.current_screen
        next = Screen(name=name)
        self.button = Button(text=message)
        next.add_widget(self.button)
        self.sm.add_widget(next)
        self.sm.current = name
        if last is not None:
            self.sm.remove_widget(last)
    def append_block(self, message):
        self.display_block(self.sm.current + ' ...', self.message + '\n\n' + message)
    def on_confirm(self, object, value):
        self.cv.acquire()
        self.confirm = True
        self.cv.notify_all()
        self.cv.release()
    def confirm_to(self, action):
        self.cv.acquire()
        self.append_block('Press to ' + action + '.')
        self.button.bind(on_touch_down=self.on_confirm)
        self.confirm = False
        while not self.confirm:
            self.cv.wait()
            if self.stop:
                raise SystemExit()
        self.cv.release()

if __name__ == '__main__':
    HeroPath().run()
else:
    ui = HeroPath().run
