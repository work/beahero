import os

try:
    input = raw_input
except NameError:
    pass

class ui:
    def __init__(self):
        import main
        main.beahero(self)
    def datadir(self):
        return os.path.expanduser(os.path.join('~','.local'))
    def display_block(self, name, message):
        print('')
        print('##### ' + name + ' #####')
        self.append_block(message)
    def append_block(self, message):
        print('')
        print(message)
        print('')
    def confirm_to(self, action):
        print('Press enter to {action}.'.format(**locals()))
        response=input('')

if __name__ == '__main__':
    ui()
